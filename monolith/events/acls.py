from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    head = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    params = {"query": f"{city} {state}"}
    response = requests.get(url, headers=head, params=params)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return None


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    param_info = {
        "q": [city, state],
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    response = requests.get(url, params=param_info)
    content = json.loads(response.content)

    lat = content[0]["lat"]
    lon = content[0]["lon"]

    response = requests.get(
        f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    )
    content = response.json()
    description = content["weather"][0]["main"]
    temp = content["main"]["temp"]
    return {"description": description, "temp": temp}
